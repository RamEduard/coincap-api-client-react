# CoinCap API
React for render API Data from https://coincap.io

### Example usage

```
	<style>
	    .changed-up,
      .changed-down {
          -webkit-transition: all 500ms cubic-bezier(0.420, 0.000, 0.580, 1.000);
          -moz-transition: all 500ms cubic-bezier(0.420, 0.000, 0.580, 1.000);
          -o-transition: all 500ms cubic-bezier(0.420, 0.000, 0.580, 1.000);
          transition: all 500ms cubic-bezier(0.420, 0.000, 0.580, 1.000); /* ease-in-out */

          -webkit-transition-timing-function: cubic-bezier(0.420, 0.000, 0.580, 1.000);
          -moz-transition-timing-function: cubic-bezier(0.420, 0.000, 0.580, 1.000);
          -o-transition-timing-function: cubic-bezier(0.420, 0.000, 0.580, 1.000);
          transition-timing-function: cubic-bezier(0.420, 0.000, 0.580, 1.000); /* ease-in-out */
      }
      .changed-up {
          background-color: #00c7d7;
      }
      .changed-down {
          color: #fff;
          background: #001641;
      }
  </style>

	<div id="coin-cap-api" />
	<script src="https://gitlab.com/RamEduard/coincap-api-client-react/raw/master/dist/coin-cap-api.min.js"></script>
```