import React from 'react';
import $ from 'jquery';
import io from 'socket.io-client';

const ENDPOINT = 'http://www.coincap.io';

const socket = io('http://socket.coincap.io');

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

const Item = (props) => {
    if (!props['item']) {
        return null;
    }

    let item = props.item,
        counter = props.counter;

    return (
        <tr className={`coin-cap-item ${item.classes}`}>
            <td>{counter}</td>
            <td>{item.long}</td>
            <td>${numberWithCommas(Number(item.mktcap).toFixed(3))}</td>
            <td>${numberWithCommas(Number(item.price).toFixed(4))}</td>
            <td>${Number(item.vwapData).toFixed(4)}</td>
            <td>{numberWithCommas(item.supply)}</td>
            <td>${numberWithCommas(item.usdVolume)}</td>
            <td>{item.cap24hrChange}%</td>
            <td>{(item.shapeshift) ? 'Buy / Sell' : ''}</td>
        </tr>
    )
};

export default class CoinCapFront extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            error: false,
            errorMsg: '',
            items: []
        }
    }

    componentWillMount() {
        this.requestApi();
    }

    componentDidMount() {
        socket.on('global', function (globalMsg) {
            this.requestApi();
        }.bind(this))

        socket.on('trade', function (lastest) {
            this.requestApi();
        }.bind(this))
    }

    requestApi() {
        var _this = this;

        $.ajax({
            url: ENDPOINT + '/front',
            dataType: 'json',
            beforeSend: () => {
                $('.coin-cap-front > .front-layer').show()
                $('.coin-cap-front table.responsive-table > tbody').css('opacity', '0.2')
            },
            error: () => {
                _this.updateError('Has been ocurred an error.')
            },
            success: (data, textStatus, xhr) => {
                _this.updateItems(data)
                $('.coin-cap-front > .front-layer').hide()
                $('.coin-cap-front table.responsive-table > tbody').css('opacity', '1')

                //// Items
                var $coinItem = $('.coin-cap-front table.responsive-table > tbody > tr.coin-cap-item.changed-down, .coin-cap-front table.responsive-table > tbody > tr.coin-cap-item.changed-up');

                $coinItem
                    .animate({
                        opacity: .5
                    }, 4000, () => {
                        $coinItem.css({
                            opacity: ''
                        }).removeClass('changed-down changed-up')
                    })
            }
        })
    }

    render() {
        let itemsRendered = this.state.items.map((item, key) => {
            return <Item key={key} counter={key+1} item={item} />
        });

        return (
            <div className="coin-cap-front" style={{position: 'relative'}}>
                <div
                    className="back-layer"
                    style={{
                        height: '100%'
                    }}
                    >
                    <table className="responsive-table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Market Cap</th>
                            <th>Price</th>
                            <th>24h VWAP</th>
                            <th>Available Supply</th>
                            <th>24h Volume</th>
                            <th>% 24h</th>
                            <th>Trade</th>
                        </tr>
                        </thead>
                        <tbody>
                        {itemsRendered}
                        </tbody>
                    </table>
                </div>
                <div
                    className="front-layer"
                    style={{
                        display: 'none',
                        position: 'absolute',
                        width: '100%',
                        textAlign: 'center',
                        top: '48.5%',
                        zIndex: 1
                    }}
                    >
                    <div className="preloader-wrapper big active">
                        <div className="spinner-layer" style={{borderColor: '#00c7d7'}}>
                            <div className="circle-clipper left">
                                <div className="circle"></div>
                            </div>
                            <div className="gap-patch">
                                <div className="circle"></div>
                            </div>
                            <div className="circle-clipper right">
                                <div className="circle"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    updateItems(items) {
        let counter = 0,
            maxItems = 10,
            oldItems = this.state.items,
            newItems = [];

        for (counter; counter < maxItems; counter++) {
            var oldItem = oldItems[counter] || {},
                newItem = items[counter],
                statement = JSON.stringify(oldItem) === JSON.stringify(newItem);

            newItem['classes'] = statement ? 'not-changed' : 'changed';

            if (oldItem['perc'] != newItem['perc']) {
                newItem['classes'] += (oldItem['perc'] > newItem['perc']) ? '-down' : '';
                newItem['classes'] += (oldItem['perc'] < newItem['perc']) ? '-up' : '';

                newItems.push(newItem);
            } else {
                newItems.push(oldItem)
            }
        }

        this.setState({
            items: newItems
        })
    }

    updateError(message) {
        this.setState({
            error: true,
            errorMsg: message
        })
    }
}