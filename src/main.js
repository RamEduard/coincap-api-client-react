import React from 'react';
import ReactDOM from 'react-dom';

import CoinCapFront from './components/CoinCapFront';

ReactDOM.render(
    <CoinCapFront />,
    document.getElementById('coin-cap-api')
);